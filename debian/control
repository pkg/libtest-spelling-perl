Source: libtest-spelling-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: hunspell <!nocheck>,
                     hunspell-en-us <!nocheck>,
                     libipc-run3-perl <!nocheck>,
                     libpod-spell-perl <!nocheck>,
                     libtest-pod-perl <!nocheck>,
                     libtest-simple-perl <!nocheck>,
                     perl
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libtest-spelling-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libtest-spelling-perl.git
Homepage: https://metacpan.org/release/Test-Spelling
Rules-Requires-Root: no

Package: libtest-spelling-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libipc-run3-perl,
         libpod-spell-perl
Recommends: hunspell | aspell | ispell | spell,
            hunspell-en-us | aspell-en | iamerican
Description: Perl module for spellchecking pod formatted text
 Test::Spelling lets you check the spelling of a POD file, and report
 its results in standard Test::More fashion. This module requires a
 spellcheck program such as spell, aspell, ispell, or hunspell.
 .
 You can add your own stopwords, which are words that should be ignored by the
 spell check. See libpod-spell-perl (which this module is built upon) for a
 variety of ways to add per-file stopwords to each .pm file. If you have a lot
 of stopwords, it's useful to put them in your test file's DATA section.
